Installing the dependencies is a little bit difficult.

Get sure to install first numpy:

    pip install numpy

And then gdal with following command:

    pip install GDAL==$(gdal-config --version) --global-option=build_ext --global-option="-I/usr/include/gdal"